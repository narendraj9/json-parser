module Main where

import Lib

main :: IO ()
main = do
  putStrLn "Enter text to parse:"
  parsedJSON <- toJSON <$> getLine
  putStrLn ("Parsed JSON: \n" ++ show parsedJSON)
  main

module JSON (JSONValue (..)
            , toJSONValue
            ) where

import           Text.Parsec
import           Text.Parsec.Char
import           Text.Parsec.Combinator
import qualified Data.Map as Map

type Parser = Parsec String () JSONValue
type Number  = Double
type Key     = String

-- As defined at https://www.json.org/
data JSONValue = Object (Map.Map Key JSONValue)
               | Array [JSONValue]
               | Number Number
               | String String
               | Boolean Bool
               | Null
  deriving (Show, Eq)

-- Helpers
-- Parser for a character allowing spaces around it.
laxChar :: Char -> Parsec String () Char
laxChar c = spaces *> char c <* spaces

-- Similar to `laxChar` for parsing a fixed string
laxString :: String -> Parsec String () String
laxString s = spaces *> string s <* spaces

comma :: Parsec String () Char
comma = laxChar ','

colon :: Parsec String () Char
colon = laxChar ':'
--

-- Doesn't support base-exponent notation yet.
-- Always adds a .0 to the number unlike JSON numbers.
numberParser :: Parser
numberParser = Number <$> fmap read (try digiWithDeci <|> digiSeq)
  where digiSeq = many1 digit
        digiWithDeci = do initDigis <- digiSeq
                          char '.'
                          fracDigis <- digiSeq
                          return (initDigis ++ "." ++ fracDigis)

nullParser :: Parser
nullParser = laxString  "null" *> return Null

boolParser :: Parser
boolParser = Boolean <$> (true <|> false)
  where true  = (laxString "true" *>  return True)
        false = (laxString "false" *> return False)

-- It's this ugly to handle strings like this ↓
-- parse stringParser' "in" "\"testing\\\"ithere\""
-- If we do not have to worry about this, we can have:
-- stringParser' = between quote quote innerParser
--   where innerParser = (many (noneOf "\""))
--         quote = char '"'
stringParser' = char '"' *> parseRest
  where parseRest = do{ c <- try $ (noneOf "\\") <* char '"'; return [c] } <|>
                    do{ x <- anyChar; xs <- parseRest; return (x:xs) }


stringParser :: Parser
stringParser = String <$> stringParser'

arrayParser :: Parser
arrayParser = Array <$> between openBracket closeBracket parseItems
  where parseItems = jsonParser `sepBy` comma
        openBracket = laxChar '['
        closeBracket = laxChar ']'

objectParser :: Parser
objectParser = Object . Map.fromList <$> between openBrace closeBrace kvPairsParser
  where
    -- Couldn't find a way to do this with Applicative only.
    kvPairParser = do k <- stringParser'
                      colon
                      value <- jsonParser
                      return (k, value)
    kvPairsParser = kvPairParser `sepBy` comma
    openBrace = laxChar '{'
    closeBrace = laxChar '}'


jsonParser :: Parser
jsonParser = objectParser <|>
             arrayParser  <|>
             boolParser   <|>
             numberParser <|>
             stringParser <|>
             nullParser

-- toJSONValue "[{\"name\":\"Narendra Joshi\",\"age\":28,  \"wtf\\\"-is-this?\":22}]"
toJSONValue :: String -> Either ParseError JSONValue
toJSONValue = parse jsonParser "input-string"

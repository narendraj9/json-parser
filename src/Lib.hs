module Lib
    ( toJSON
    ) where

import Text.Parsec
import JSON

toJSON :: String -> Either ParseError JSONValue
toJSON = toJSONValue
